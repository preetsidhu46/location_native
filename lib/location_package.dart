import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LocationPackage {
  static const MethodChannel _channel =
      const MethodChannel('com.hawkeye.bgservice');

  Function callbackFunction;
  LocationPackage({@required Function callback}) {
    callbackFunction = callback;
    _channel.setMethodCallHandler((m) => setupMethodHandler(m));
  }

  setupMethodHandler(m) async {
    String channelName = m.method.trim();
    switch (channelName) {
      case "locationUpdated":
        print("location locationUpdated");
        callbackFunction(
            methodName: channelName,
            arguments: {'lat': m.arguments['lat'], 'lng': m.arguments['lng']});
        break;
      case "locationSpoofed":
        callbackFunction(methodName: channelName);
        break;
      default:
    }
  }

  Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  locationSpoofingDisable() async {
    print("locationSpoofingDisable");
    await _channel.invokeMethod("locationSpoofingDisable");
  }

  locationSpoofingEnable() async {
    print("locationSpoofingDisable");
    await _channel.invokeMethod("locationSpoofingEnable");
  }

  locationServiceStart(Map locationServiceData) async {
    print("in page background 4=> $locationServiceData");
    await _channel.invokeMethod("locationServiceStart", locationServiceData);
  }

  locationServiceStop() async {
    await _channel.invokeMethod("locationServiceEnd");
  }

  Future<Map> startOneTimeLocationPing() async {
    Map value = await _channel.invokeMethod("oneTimeLocationPing");
    print("one time location ==================> ${value.toString()}");
    return value;
  }

  Future<bool> isLocationEnable() async {
    bool value = await _channel.invokeMethod("checkLocationPerm");
    return value;
  }
}
