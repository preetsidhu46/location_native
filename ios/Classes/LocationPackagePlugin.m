#import "LocationPackagePlugin.h"
#import <location_package/location_package-Swift.h>

@implementation LocationPackagePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftLocationPackagePlugin registerWithRegistrar:registrar];
}
@end
