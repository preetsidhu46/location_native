package com.bounceshare.location_package.api;

import com.google.gson.JsonObject;

import retrofit2.Callback;

public interface NetworkServices {
    void sendLocation(String contentType, String authToken, String url, String agentId, JsonObject request, Callback<UserData> listener);

}
