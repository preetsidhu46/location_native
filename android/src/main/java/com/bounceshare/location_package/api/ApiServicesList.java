package com.bounceshare.location_package.api;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface ApiServicesList {
    @POST
    Call<UserData> sendLocation(@Header("Content-Type") String contentType,
                                @Header("authorization") String authToken,
                                @Header("agent_id") String agentId,
                                @Url String url,
                                @Body JsonObject body

    );
}
