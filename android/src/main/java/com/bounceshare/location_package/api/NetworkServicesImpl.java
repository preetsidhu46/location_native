package com.bounceshare.location_package.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkServicesImpl implements NetworkServices {
    private static Gson gson = new GsonBuilder()
            .setLenient()
            .create();
    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(300, TimeUnit.SECONDS)
            .connectTimeout(360, TimeUnit.SECONDS)
            .cache(null)
            .build();
    //Singleton instance
    private static volatile NetworkServicesImpl sInstance;
    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(gson));
    //APIServices instance
    ApiServicesList mAPIServices = null;
    //Retrofit instance
    private Retrofit mRetrofit = null;

    /**
     * Constructor is made private to make sure that outside classes can't instantiate it to constructor.
     * Thus sticking to the singleton pattern
     */
    private NetworkServicesImpl() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.level(HttpLoggingInterceptor.Level.BODY);
        okHttpClient = new OkHttpClient.Builder()
                .readTimeout(300, TimeUnit.SECONDS)
                .connectTimeout(360, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .cache(null)
                .build();
        mAPIServices = createService(ApiServicesList.class);
    }

    private static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(okHttpClient).baseUrl("https://hero-tracking-gateway.bounce.bike").build();
        return retrofit.create(serviceClass);
    }

    /**
     * @return Singleton instance of the ServiceHolder class
     */
    public static NetworkServices getInstance() {
        if (null == sInstance){
            synchronized (NetworkServicesImpl.class) {
                sInstance = new NetworkServicesImpl();
            }
        }
        return sInstance;
    }

    @Override
    public void sendLocation(String contentType, String authToken, String url, String agentId, JsonObject request, Callback<UserData> listener) {
        Call<UserData> call = mAPIServices
                .sendLocation(contentType, authToken,agentId, url, request);
        call.enqueue(listener);
    }
}
