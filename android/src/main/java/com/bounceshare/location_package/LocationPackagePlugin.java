package com.bounceshare.location_package;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import java.util.Map;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;

public class LocationPackagePlugin implements MethodChannel.MethodCallHandler, PluginRegistry.ActivityResultListener {
    static public Activity activity;
    static public MethodChannel methodChannel;

    /** Plugin registration. */
    public static void registerWith(PluginRegistry.Registrar registrar) {
        methodChannel = new MethodChannel(registrar.messenger(), MethodChannels.LOCATION_FETCH_CHANNEL);
        LocationPackagePlugin plugin = new LocationPackagePlugin();
        methodChannel.setMethodCallHandler(plugin);
        activity = registrar.activity();
        registrar.addActivityResultListener(plugin);
        LocationUtil.init(activity);

    }

    @Override
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        String callName = methodCall.method.trim();
        switch (callName) {
        case MethodCalls.Location.CHECK_LOCATION_PERM:
            LocationUtil.checkLocation(activity, result);
            break;
        case MethodCalls.Location.LOCATION_SPOOFING_DISABLE:
            LocationUtil.setSpoofing(activity.getApplicationContext(), false);
            result.success("done");
            break;
        case MethodCalls.Location.LOCATION_SPOOFING_ENABLE:
            LocationUtil.setSpoofing(activity.getApplicationContext(), true);
            result.success("done");
            break;
        case MethodCalls.Location.START_LOCATION_FETCH:
            Map locationServiceData = (Map) methodCall.arguments;
            Intent intent = getIntentWithExtras(locationServiceData);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                activity.startForegroundService(intent);
            } else
                activity.startService(intent);
            result.success("done");
            break;
        case MethodCalls.Location.STOP_LOCATION_FETCH:
            try {
                if (LocationUtil.isMyServiceRunning(activity, LocationFetchingService.class)) {

                    intent = new Intent(activity.getBaseContext(), LocationFetchingService.class);
                    intent.setAction("stop");
                    activity.stopService(intent);
                    new Handler().postDelayed(() -> LocationUtil.stopContinuousLocation(activity), 500);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            break;
        case MethodCalls.Location.GET_CURRENT_LOCATION:
            LocationUtil.startOneTimeLocationFetch(activity, result);
            // result.success("done");
            break;
        default:
            result.notImplemented();
        }
    }

    private Intent getIntentWithExtras(Map locationServiceData) {
        Intent intent = new Intent(activity.getBaseContext(), LocationFetchingService.class);
        intent.putExtra("agentId", "" + locationServiceData.get("agentId"));
        intent.putExtra("agentStatus", (Integer) locationServiceData.get("agentStatus"));
        intent.putExtra("taskId", "" + locationServiceData.get("taskId"));
        intent.putExtra("taskStatus", "" + locationServiceData.get("taskStatus"));
        intent.putExtra("taskLatitude", (Double) locationServiceData.get("taskLatitude"));
        intent.putExtra("taskLongitude", (Double) locationServiceData.get("taskLongitude"));
        intent.putExtra("timeout", (Integer) locationServiceData.get("minimumTime"));
        intent.putExtra("authToken", "" + locationServiceData.get("authToken"));
        intent.putExtra("apiUrl", "" + locationServiceData.get("apiUrl"));
        intent.setAction("start");
        return intent;
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("Ign Package onAResult", "" + requestCode);
        switch (requestCode) {
        case LocationUtil.REQUEST_CHECK_SETTINGS:
            Log.d("ActivityResult", "requestCode :" + requestCode);
            Log.d("ActivityResult", "resultCode : " + resultCode);

            switch (resultCode) {
            case Activity.RESULT_OK: {
                // All required changes were successfully made
                LocationUtil.handlePermissionPopup(true, activity);
                break;
            }
            case Activity.RESULT_CANCELED: {
                // The user was asked to change settings, but chose not to

                LocationUtil.handlePermissionPopup(false, activity);
                break;
            }
            default: {
                break;
            }
            }
            break;
        default:
            return false;
        }

        return true;
    }
}
