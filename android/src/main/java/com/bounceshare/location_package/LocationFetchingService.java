package com.bounceshare.location_package;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.bounceshare.location_package.api.NetworkServicesImpl;
import com.bounceshare.location_package.api.UserData;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.JsonObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bounceshare.location_package.LocationPackagePlugin.activity;

public class LocationFetchingService extends Service {
    public static SharedPreferences prefs;
    AlarmManager alarmMgr;
    PendingIntent alarmIntent;

    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    HashMap userDetails = new HashMap();
    String locationURl = "";
    OnLocationUpdatedListener listener = new OnLocationUpdatedListener() {
        @Override
        public void onLocationUpdated(Location location) {
            Log.e("print", location.toString());
            if (LocationUtil.locationSpoofingEnabled && location.isFromMockProvider()) {
                if (LocationPackagePlugin.methodChannel != null)
                    LocationPackagePlugin.methodChannel.invokeMethod("locationSpoofed", null);
            } else {
                if (LocationPackagePlugin.methodChannel != null)
                    LocationPackagePlugin.methodChannel.invokeMethod("locationUpdated", getLocationMap(location));

                pushLocationToFirebase(location);
                pushLocationToSNSServer(location);
            }
        }
    };

    private void pushLocationToSNSServer(Location location) {

        Integer agentStatus = (Integer) userDetails.get("agent_status");
        Double taskLatitude = (Double) userDetails.get("task_latitude");
        Double taskLongitude = (Double) userDetails.get("task_longitude");
        long dateTime = Calendar.getInstance().getTime().getTime();
        JsonObject paramObject = new JsonObject();
        paramObject.addProperty("authToken", userDetails.get("authToken").toString());
        paramObject.addProperty("agentId", userDetails.get("agent_id").toString());
        paramObject.addProperty("agentLatitude", location.getLatitude());
        paramObject.addProperty("agentLongitude", location.getLongitude());
        paramObject.addProperty("agentStatus", agentStatus);
        paramObject.addProperty("taskStatus", userDetails.get("task_status").toString());
        paramObject.addProperty("taskLongitude", taskLongitude);
        paramObject.addProperty("taskLatitude", taskLatitude);
        paramObject.addProperty("taskId", userDetails.get("task_id").toString());
        paramObject.addProperty("locationAccuracy", location.getAccuracy());
        paramObject.addProperty("createdAt", dateTime);

        NetworkServicesImpl.getInstance().sendLocation("application/json", "" + userDetails.get("authToken"),
                locationURl, "" + userDetails.get("agent_id"), paramObject, new Callback<UserData>() {
                    @Override
                    public void onResponse(Call<UserData> call, Response<UserData> response) {
                        Log.e("response", "" + response);

                    }

                    @Override
                    public void onFailure(Call<UserData> call, Throwable t) {
                        Log.e("response", "fail");

                    }
                });

    }

    private void pushLocationToFirebase(Location location) {
        HashMap dataMap = new HashMap();
        dataMap.put("agent_latitude", location.getLatitude());
        dataMap.put("agent_longitude", location.getLongitude());
        dataMap.put("location_accuracy", location.getAccuracy());
        dataMap.put("timestamp", Calendar.getInstance().getTime().getTime());
        dataMap.putAll(userDetails);
        Log.d("pushLocationToFirebase", "" + dataMap.toString());
        mDatabase.child("locations").push().setValue(dataMap);
    }

    private Map getLocationMap(Location location) {
        Map map = new HashMap();
        map.put("lat", location.getLatitude());
        map.put("lng", location.getLongitude());
        return map;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Service", "onCreate");
        IntentFilter filter = new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION);
        filter.addAction(Intent.ACTION_PROVIDER_CHANGED);
        this.registerReceiver(gpsSwitchStateReceiver, filter);
    }

    @Override
    public IBinder onBind(Intent intent) {
        try {
            return null;
        } catch (Exception e) {

        }
        return null;
    }

    @SuppressLint("MissingPermission")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Service", "onStartCommand");
        try {
            if (intent.getAction().equalsIgnoreCase("start")) {
                setUserData(intent);
                int timeout = intent.getExtras().getInt("timeout");
                try {
                    if (alarmMgr != null) {
                        Intent intentD = new Intent(getApplicationContext(), BroadcastHandler.class);
                        PendingIntent alarmIntent;
                        alarmIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intentD,
                                PendingIntent.FLAG_CANCEL_CURRENT);
                        alarmMgr.cancel(alarmIntent);

                    }
                    startHacking();
                    // LocationUtil.startContinuousLocation(this, listener);
                    startForeground(100, getNotification());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return START_REDELIVER_INTENT;
            } else {
                LocationUtil.stopContinuousLocation(this);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("stop alarm", "stop alarm run");
                        stopSelf();
                    }
                }, 500);
                // new Handler().postDelayed(this::stopSelf,500);
                try {
                    this.unregisterReceiver(gpsSwitchStateReceiver);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                return START_NOT_STICKY;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return flags;
    }

    private void startHacking() {
        Log.e("hack start", "hackstart");
        alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), BroadcastHandler.class);
        intent.putExtra("dataMap", userDetails);
        Log.e("url sent 2", locationURl);
        intent.putExtra("locationURl", locationURl);
        alarmIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.e("hack start", "hackdone");
            alarmMgr.setRepeating(AlarmManager.RTC, SystemClock.elapsedRealtime(), 60000, alarmIntent);
        }

    }

    private Notification getNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,
                MethodChannels.NOTIFICATION_CHANNEL_ID).setContentText("Location Service is running")
                        .setSmallIcon(R.drawable.notification).setContentTitle("Bounce Hero").setOngoing(true);
        return builder.build();
    }

    private void setUserData(Intent intent) {
        locationURl = intent.getExtras().getString("apiUrl");
        userDetails.put("agent_id", intent.getExtras().getString("agentId"));
        userDetails.put("agent_status", intent.getExtras().getInt("agentStatus"));
        userDetails.put("task_id", intent.getExtras().getString("taskId"));
        userDetails.put("task_status", intent.getExtras().getString("taskStatus"));
        userDetails.put("task_longitude", intent.getExtras().getDouble("taskLongitude"));
        userDetails.put("task_latitude", intent.getExtras().getDouble("taskLatitude"));
        userDetails.put("authToken", intent.getExtras().getString("authToken"));
    }

    @Override
    public void onDestroy() {
        try {
            this.unregisterReceiver(gpsSwitchStateReceiver);
            Intent intentD = new Intent(getApplicationContext(), BroadcastHandler.class);
            PendingIntent alarmIntent;
            alarmIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intentD,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            alarmMgr.cancel(alarmIntent);
            Log.e("stop alarm", "stop alarm");
            // unregisterReceiver(mReciever);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        super.onDestroy();

        Log.d("Service", "Service Destroyed");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e("Service", "onTaskRemoved");
        super.onTaskRemoved(rootIntent);
    }

    private BroadcastReceiver gpsSwitchStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (LocationManager.PROVIDERS_CHANGED_ACTION.equals(intent.getAction())) {
                // Make an action or refresh an already managed state.

                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                if (isGpsEnabled || isNetworkEnabled) {
                    // Log.i(this.getClass().getName(), "gpsSwitchStateReceiver.onReceive() location
                    // is enabled : isGpsEnabled = "
                    // + isGpsEnabled + " isNetworkEnabled = " + isNetworkEnabled);

                } else {
                    Log.w(this.getClass().getName(), "gpsSwitchStateReceiver.onReceive() location disabled ");
                    LocationUtil.checkLocation(activity, null);
                }
            }
        }
    };
}
