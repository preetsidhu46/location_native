package com.bounceshare.location_package;
public class MethodCalls {
    public interface Location {
        String START_LOCATION_FETCH = "locationServiceStart";
        String GET_CURRENT_LOCATION = "oneTimeLocationPing";
        String STOP_LOCATION_FETCH = "locationServiceEnd";
        String CHECK_LOCATION_PERM = "checkLocationPerm";
        String LOCATION_SPOOFING_DISABLE = "locationSpoofingDisable";
        String LOCATION_SPOOFING_ENABLE = "locationSpoofingEnable";
    }
}
