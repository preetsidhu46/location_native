package com.bounceshare.location_package;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.bounceshare.location_package.api.NetworkServicesImpl;
import com.bounceshare.location_package.api.UserData;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.JsonObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationAccuracy;
import io.nlopez.smartlocation.location.config.LocationParams;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BroadcastHandler extends BroadcastReceiver {
    private static LocationParams getConfig() {
        return new LocationParams.Builder().setAccuracy(LocationAccuracy.HIGH).setInterval(80000).setDistance(0)
                .build();
    }

    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

    // Bundle userDetails = new Bundle();
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("hack start", "hackstart out");

        final Map<String, Double> map = new HashMap<>();

        SmartLocation.with(context).location().config(getConfig()).oneFix().start(new OnLocationUpdatedListener() {
            @Override
            public void onLocationUpdated(Location location) {
                map.put("latitude", location.getLatitude());
                map.put("longitude", location.getLongitude());
                try {
                    Log.e("location from hack",
                            location.getAccuracy() + "," + location.getLatitude() + "," + location.getLongitude());
                    HashMap userDetails = (HashMap) intent.getSerializableExtra("dataMap");
                    Log.e("data", userDetails.toString());
                    Log.e("url", intent.getExtras().getString("locationURl"));
                    String urlData = (String) intent.getExtras().getString("locationURl");
                    if (LocationUtil.locationSpoofingEnabled && location.isFromMockProvider()) {
                        if (LocationPackagePlugin.methodChannel != null)
                            LocationPackagePlugin.methodChannel.invokeMethod("locationSpoofed", null);
                    } else {
                        if (LocationPackagePlugin.methodChannel != null)
                            LocationPackagePlugin.methodChannel.invokeMethod("locationUpdated",
                                    getLocationMap(location));
                        Log.e("url", urlData);
                        pushLocationToFirebase(userDetails, location);
                        pushLocationToSNSServer(userDetails, location, urlData);
                    }
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void startService(Context context, String action) {
        Log.e("hack start", "hackstart start service");

    }

    private Map getLocationMap(Location location) {
        Map map = new HashMap();
        map.put("lat", location.getLatitude());
        map.put("lng", location.getLongitude());
        return map;
    }

    private void pushLocationToSNSServer(HashMap userDetails, Location location, String locationURL) {
        Log.e("url", locationURL.toString());
        Log.e("user Details", userDetails.toString());
        Integer agentStatus = (Integer) userDetails.get("agent_status");
        Double taskLatitude = (Double) userDetails.get("task_latitude");
        Double taskLongitude = (Double) userDetails.get("task_longitude");
        long dateTime = Calendar.getInstance().getTime().getTime();
        JsonObject paramObject = new JsonObject();
        paramObject.addProperty("authToken", userDetails.get("authToken").toString());
        paramObject.addProperty("agentId", userDetails.get("agent_id").toString());
        paramObject.addProperty("agentLatitude", location.getLatitude());
        paramObject.addProperty("agentLongitude", location.getLongitude());
        paramObject.addProperty("agentStatus", agentStatus);
        paramObject.addProperty("taskStatus", userDetails.get("task_status").toString());
        paramObject.addProperty("taskLongitude", taskLongitude);
        paramObject.addProperty("taskLatitude", taskLatitude);
        paramObject.addProperty("taskId", userDetails.get("task_id").toString());
        paramObject.addProperty("locationAccuracy", location.getAccuracy());
        paramObject.addProperty("createdAt", location.getTime());

        NetworkServicesImpl.getInstance().sendLocation("application/json", "" + userDetails.get("authToken"),
                locationURL, "" + userDetails.get("agent_id"), paramObject, new Callback<UserData>() {
                    @Override
                    public void onResponse(Call<UserData> call, Response<UserData> response) {
                        Log.e("response", "" + response);

                    }

                    @Override
                    public void onFailure(Call<UserData> call, Throwable t) {
                        Log.e("response", "fail" + t.toString());

                    }
                });

    }

    private void pushLocationToFirebase(HashMap userDetails, Location location) {
        HashMap dataMap = new HashMap();
        dataMap.put("agent_latitude", location.getLatitude());
        dataMap.put("agent_longitude", location.getLongitude());
        dataMap.put("location_accuracy", location.getAccuracy());
        dataMap.put("timestamp", Calendar.getInstance().getTime().getTime());
        dataMap.putAll(userDetails);
        Log.d("pushLocationToFirebase", "" + dataMap.toString());
        mDatabase.child("locations").push().setValue(dataMap);
    }
}
